nom_fichier = input("Quel fichier souhaitez vous lire? ")

try:
    nombre = int(input("Combien de fois doit-on imprimer le fichier? "))
    print(f"Le fichier {nom_fichier} va être imprimé {nombre} fois.")   

    with open(nom_fichier) as fp:
        lines = fp.readlines()
        for n in range(nombre):
            for line in lines:
                print(line.strip())
except ValueError:
    print("Désolé mais vous devez entrer un nombre!")
except Exception:
    print("Désolé le fichier en question n'existe pas!")
else:
    print("Tout c'est bien passé!")
finally:
    print("Ce code s'executera peu importe le résultat du try!")

print("Merci d'avoir utilisé notre programme.")